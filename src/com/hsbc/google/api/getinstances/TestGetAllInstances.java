package com.hsbc.google.api.getinstances;

import java.io.FileWriter;
import java.io.IOException;

import org.json.*;

public class TestGetAllInstances {

	public static void main(String[] args) throws IOException, JSONException {
		// TODO Auto-generated method stub
		
		FileWriter fw = null;
		
		System.out.println("Starting to delete instance");
		
		GetAllInstancesList newComputeEngine = new GetAllInstancesList(
				"EEP Hackathon",
				"lively-epsilon-170708",
				"asia-southeast1-a",
				"my-sample-instance"
				);
		
		JSONObject json = newComputeEngine.computeInstance();
		
		try {
			fw = new FileWriter("./Output/out.json");
			fw.write(json.toString(4));
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			fw.close();
		}
		
		System.out.println("Completed.");
	}

}
