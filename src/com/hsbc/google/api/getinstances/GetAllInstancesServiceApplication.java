package com.hsbc.google.api.getinstances;


import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@EnableDiscoveryClient
@SpringBootApplication
public class GetAllInstancesServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(GetAllInstancesServiceApplication.class, args);
	}
}

@RefreshScope
@RestController
class DeleteInstanceRestController{
	
	

	@RequestMapping(value="/listInstances", method=RequestMethod.POST)
	public org.json.JSONObject listInstances(@RequestBody GetAllInstancesList listInstancesComputeEngine){
		
		GetAllInstancesList newComputeEngine = new GetAllInstancesList(
				listInstancesComputeEngine.getApplicationName(),
				listInstancesComputeEngine.getProjectId(),
				listInstancesComputeEngine.getZoneName(), 
				listInstancesComputeEngine.getSampleInstanceName());
		
		return newComputeEngine.computeInstance();
	}
}

